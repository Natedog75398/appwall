"use strict";

Template.posts.helpers({
    postList: function () {
        return Collections.Images.find({});
    },

    updateMasonry: function () {
        $('.grid').imagesLoaded().done(function () {
            $('.grid').masonry({
                itemSelector: '.grid-item',
                gutter: 15,
                columnWidth: '.grid-sizer',
                percentPostion: true
            });
        });
    }
});