Collections = {};

var imageStore = new FS.Store.GridFS("image");

Collections.Images = new FS.Collection("images", {
    stores: [imageStore]
});